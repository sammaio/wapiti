# -*- coding: utf-8 -*-
##
##
#
# Start the nmap scan and wait for it to complete.
# Then format the resonse and send the data back to the server
#
import subprocess
import os
import json
import csv
import time




def start_scan():
    '''
    Start the nmap scan of the target
    '''
    print("Starting wapi scan")
    process = subprocess.Popen('wapiti/bin/wapiti -u {0} -o /report.json -f json --flush-session'.format(os.environ['TARGET']) , shell=True, stdout=subprocess.PIPE)
    process.wait()   
    





def convert_output():
    '''
    Converts the output to json
    '''

    with open('/report.json') as f:
        d = json.load(f)
        out={}
        out['type']="wapiti"
        out['info']=d['infos']
        out['vulnerabilities']=d['vulnerabilities']
        print(out)
        
#Start the scan and get the output

def run():
        '''
        Sometime wapiti will not start on the first run so we are testing it 
        '''
        while True:
             if os.path.isfile('/report.json'):
                     break
             else:
                start_scan()
                time.sleep(30)

        #We have to file and can output
        convert_output()


#Start the scanner
run()
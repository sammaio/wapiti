from ubuntu
ENV LANG=C.UTF-8

#Install Wapiti
RUN apt-get update && apt-get install wget git python3-pip -y
WORKDIR /opt/
COPY  ./code /code


WORKDIR /code/wapiti
RUN python3 setup.py install

WORKDIR /code/
CMD python3 scanner.py
